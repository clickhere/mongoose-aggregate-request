var xtend = require("xtend");

function aggregate(model, columnMap, request, options, cb) {
    options = options || {};

    var handlers = [
        handleDateRange,
        handleFilter,
        handleColumns.bind(null, columnMap),
        handleSort,
        handleSortObject,
        handleSkip,
        handleLimit
    ];

    handlers = handlers.concat(options.handlers || []);

    handleRequest(model, handlers, request).exec(function (err, docs) {
        cb(err, docs || []);
    });
}

function aggregateCount(model, columnMap, request, options, cb) {

    var handlers = [
        handleDateRange,
        handleFilter,
        handleColumns.bind(null, columnMap)
    ];

    handleRequest(model, handlers, request).exec(function (err, docs) {
        cb(err, (docs || []).length);
    });
}

function handleRequest(model, handlers, request) {
    return handlers.reduce(function (acc, handler) {
        return handler(request, acc);
    }, model.aggregate());
}

function handleSkip(request, builder) {
    if (request && request.skip && isInt(request.skip))
        return builder.skip(request.skip);
    return builder;
}

function handleLimit(request, builder) {
    if (request && request.limit && isInt(request.limit))
        return builder.limit(request.limit);
    return builder;
}

function handleSort(request, builder) {
    if (request && request.sort && Array.isArray(request.sort)) {
        return builder.sort(_handleSortQuery(request, {}));
    }

    return builder;
}

function _handleSortQuery(request, query) {
    return request.sort.reduce(function (acc, param) {
        param[0] === '-' ? acc[param.slice(1)] = -1 : acc[param] = 1;
        return acc;
    }, query);
}

function handleSortObject(request, builder) {
    if (request && request.sort && typeof request.sort === "object") {
        return builder.sort(_handleSortObjectQuery(request, {}));
    }

    return builder;
}

function _handleSortObjectQuery(request, query) {
    return Object.keys(request.sort).reduce(function (acc, param) {
        acc[param] = +request.sort[param];
        return acc;
    }, query);
}

function handleDateRange(request, builder) {
    if (request && request.date && Array.isArray(request.date) && request.date.length > 0) {

        var start = +new Date(request.date[0]),
            end = request.date.length > 1 ? +new Date(request.date[1]) : Date.now();

        return builder.match({
            'date': {
                $gte: start,
                $lte: end
            }
        });
    }

    return builder;
}

function handleFilter(request, builder) {
    if (request && request.filter && typeof  request.filter === "object") {

        var query = Object.keys(request.filter).map(function (param) {
            var temp = {};
            temp[param] = request.filter[param];
            return temp;
        });

        return builder.match({
            $and: query
        });
    }

    return builder;
}


function handleColumns(columnMap, request, builder) {
    if (request && request.columns && Array.isArray(request.columns)) {
        return builder.project(_handleColumns(columnMap, request.columns));
    }
    return builder;
}

function _handleColumns(columnMap, columns) {
    return columns.reduce(function (acc, column) {
        return xtend(acc, columnMap[column]);
    }, {});
}

function isInt(value) {
    return typeof value === "number" &&
        isFinite(value) &&
        Math.floor(value) === value;
}

module.exports = {
    aggregate: aggregate,
    aggregateCount: aggregateCount
};